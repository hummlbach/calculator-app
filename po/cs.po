# Czech translation for ubuntu-calculator-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-calculator-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calculator-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-08 16:37+0100\n"
"PO-Revision-Date: 2018-10-30 14:44+0000\n"
"Last-Translator: Milan Korecký <milan.korecky@gmail.com>\n"
"Language-Team: Czech <https://translate.ubports.com/projects/ubports/"
"calculator-app/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 3.0.1\n"
"X-Launchpad-Export-Date: 2017-04-05 07:42+0000\n"

#: ../app/engine/formula.js:176
msgid "NaN"
msgstr "NaN"

#: ../app/ubuntu-calculator-app.qml:267
#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:1
msgid "Calculator"
msgstr "Kalkulačka"

#: ../app/ubuntu-calculator-app.qml:271 ../app/ubuntu-calculator-app.qml:276
msgid "Favorite"
msgstr "Oblíbené"

#: ../app/ubuntu-calculator-app.qml:323
msgid "Cancel"
msgstr "Zrušit"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select All"
msgstr "Vybrat vše"

#: ../app/ubuntu-calculator-app.qml:333
msgid "Select None"
msgstr "Nevybrat žádný"

#: ../app/ubuntu-calculator-app.qml:340 ../app/ubuntu-calculator-app.qml:404
msgid "Copy"
msgstr "Kopírovat"

#: ../app/ubuntu-calculator-app.qml:348 ../app/ubuntu-calculator-app.qml:450
msgid "Delete"
msgstr "Smazat"

#: ../app/ubuntu-calculator-app.qml:414
msgid "Edit"
msgstr "Upravit"

#: ../app/ubuntu-calculator-app.qml:428
msgid "Add to favorites"
msgstr "Přidat do oblíbených"

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid
#. expressions
#: ../app/ubuntu-calculator-app.qml:566 ../app/ui/FavouritePage.qml:89
#: ../app/ui/Screen.qml:51
msgid "dd MMM yyyy"
msgstr "dd MMM yyyy"

#: ../app/ui/FavouritePage.qml:42
msgid "No favorites"
msgstr "Žádné oblíbené položky"

#: ../app/ui/FavouritePage.qml:43
msgid ""
"Swipe calculations to the left\n"
"to mark as favorites"
msgstr ""
"K označení oblíbených výpočtů \n"
"přetáhněte je doleva"

#. TRANSLATORS Natural logarithm symbol (logarithm to the base e)
#: ../app/ui/LandscapeKeyboard.qml:37 ../app/ui/PortraitKeyboard.qml:60
msgid "log"
msgstr "log"

#. TRANSLATORS Modulo operation: Finds the remainder after division of one number by another
#: ../app/ui/LandscapeKeyboard.qml:44 ../app/ui/PortraitKeyboard.qml:64
msgid "mod"
msgstr "režim"

#: ../app/ui/Screen.qml:37
msgid "Just now"
msgstr "Právě teď"

#: ../app/ui/Screen.qml:39
msgid "Today "
msgstr "Dnes "

#. TRANSLATORS: this is a time formatting string, see
#. http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for
#. valid expressions
#: ../app/ui/Screen.qml:43
msgid "hh:mm"
msgstr "hh:mm"

#: ../app/ui/Screen.qml:45
msgid "Yesterday"
msgstr "Včera"

#: ../app/ui/Walkthrough.qml:96
msgid "Skip"
msgstr "Přeskočit"

#: ../app/welcomewizard/Slide1.qml:27
msgid "Welcome to Calculator"
msgstr "Vítejte v Kalkulačce"

#: ../app/welcomewizard/Slide1.qml:28
msgid "Enjoy the power of math by using Calculator"
msgstr "Užívejte si sílu matematiky s Kalkulačkou"

#: ../app/welcomewizard/Slide10.qml:27
msgid "Copy formula"
msgstr "Kopírovat vzorec"

#: ../app/welcomewizard/Slide10.qml:28
msgid "Long press to copy part or all of a formula to the clipboard"
msgstr "Delším přitlačením zkopírujte část nebo celý vzorec do schránky"

#: ../app/welcomewizard/Slide11.qml:79
msgid "Enjoy"
msgstr "Užijte si to"

#: ../app/welcomewizard/Slide11.qml:88
msgid "We hope you enjoy using Calculator!"
msgstr "Věříme, že se vám používání kalkulačky líbí!"

#: ../app/welcomewizard/Slide11.qml:106
msgid "Finish"
msgstr "Dokončit"

#: ../app/welcomewizard/Slide2.qml:27
msgid "Scientific keyboard"
msgstr "Vědecká klávesnice"

#: ../app/welcomewizard/Slide2.qml:28
msgid "Access scientific functions with a left swipe on the numeric keypad"
msgstr "K použití vědeckých funkcí přetáhněte numerickou klávesnici doleva"

#: ../app/welcomewizard/Slide3.qml:27
msgid "Scientific View"
msgstr "Vědecký režim"

#: ../app/welcomewizard/Slide3.qml:28
msgid "Rotate device to show numeric and scientific functions together"
msgstr "Pro zobrazení číselných i vědeckých funkcí zároveň otočte zařízení"

#: ../app/welcomewizard/Slide4.qml:27
msgid "Delete item from calculation history"
msgstr "Smazat záznam z historie výpočtů"

#: ../app/welcomewizard/Slide4.qml:28
msgid "Swipe right to delete items from calculator history"
msgstr "Přetažením doprava smažete záznam z historie výpočtů"

#: ../app/welcomewizard/Slide5.qml:27
msgid "Delete several items from calculation history"
msgstr "Smazat více záznamů z historie výpočtů"

#: ../app/welcomewizard/Slide5.qml:28
msgid "Long press to select multiple calculations for deletion"
msgstr "Delší stisk pro výběr většího množství záznamů ke smazání"

#: ../app/welcomewizard/Slide6.qml:27
msgid "Delete whole formula at once"
msgstr "Smazat celý vzorec najednou"

#: ../app/welcomewizard/Slide6.qml:28
msgid "Long press '←' button to clear all formulas from input bar"
msgstr "Delší stisk '←' pro vymazání všech vzorců ze vstupního panelu"

#: ../app/welcomewizard/Slide7.qml:27
msgid "Edit item from calculation history"
msgstr "Upravit záznam z historie výpočtů"

#: ../app/welcomewizard/Slide7.qml:28
msgid "Swipe left and press pencil to edit calculation"
msgstr "Pro úpravu výpočtu přetáhněte doleva a stiskněte tužku"

#: ../app/welcomewizard/Slide8.qml:27
msgid "Add new favourite"
msgstr "Přidat do oblíbených"

#: ../app/welcomewizard/Slide8.qml:28
msgid "Swipe left and press star to add calculations to favourites view"
msgstr "Pro přidání do oblíbených přetáhněte doleva a stiskněte hvězdičku"

#: ../app/welcomewizard/Slide9.qml:27
msgid "Edit formula"
msgstr "Upravit vzorec"

#: ../app/welcomewizard/Slide9.qml:28
msgid "Click in the middle of a formula to edit in place"
msgstr "Pro úpravu vzorce klikněte uprostřed vzorce"

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:2
msgid "A calculator for Ubuntu."
msgstr "Kalkulačka pro Ubuntu."

#: /tmp/tmp.WzlPraBXv6/po/ubuntu-calculator-app.desktop.in.in.h:3
msgid "math;addition;subtraction;multiplication;division;"
msgstr "matematika;sčítání;dělení;odčítání;násobení;"
